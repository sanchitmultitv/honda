import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FetchDataService } from 'src/app/services/fetch-data.service';

@Component({
  selector: 'app-keyportfolio-entry',
  templateUrl: './keyportfolio-entry.component.html',
  styleUrls: ['./keyportfolio-entry.component.scss']
})
export class KeyportfolioEntryComponent implements OnInit {

  constructor(private router: Router, private _fd: FetchDataService) { }
  exhibitionHall = false;
  showVideo = false;
  networkingLounge = false;

  ngOnInit(): void {
  }

  stepUpAnalytics(action) {
    let virtual: any = JSON.parse(localStorage.getItem('virtual'));
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let hrs: any = new Date().getHours();
    let mns: any = new Date().getMinutes();
    let secs: any = new Date().getSeconds();

    // alert(time)
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    if (hrs < 10) {
      hrs = '0' + hrs;
    }
    if (mns < 10) {
      mns = '0' + mns;
    }
    if (secs < 10) {
      secs = '0' + secs;
    }
    const formData = new FormData();
    formData.append('event_id', '154');
    formData.append('user_id', virtual.id);
    formData.append('name', virtual.name);
    formData.append('email', virtual.email);
    formData.append('company', 'others');
    formData.append('designation', 'others');
    formData.append('action', action);
    formData.append('created', yyyy + '-' + mm + '-' + dd + ' ' + hrs+':'+mns+':'+secs);
    this._fd.analyticsPost(formData).subscribe(res => {
      console.log('asdf', res);
    });
  }

  enterKeyPortfolio() {
    this.exhibitionHall = true;
    this.showVideo = true;
    let vid: any = document.getElementById("exhibitvideo");
    vid.play();
  }
  gotoExhibitionHallOnVideoEnd() {
    this.router.navigate(['/auditorium/two']);
  }
  enterKeyPortfolio2() {
    this.networkingLounge = true;
    this.showVideo = true;
    let vid: any = document.getElementById("netLoungevideo");
    vid.play();
  }
  gotoNetworkingLoungeOnVideoEnd() {
    this.router.navigate(['/auditorium/three']);
  }
  skipButton() {

    this.networkingLounge=false;
    this.router.navigateByUrl('/auditorium/three');
  }
  skipButton3(){
    this.exhibitionHall = false;
    this.router.navigateByUrl('/auditorium/two');
  }
}
