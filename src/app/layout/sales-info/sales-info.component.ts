import { Component, OnInit } from '@angular/core';
declare var $:any;
@Component({
  selector: 'app-sales-info',
  templateUrl: './sales-info.component.html',
  styleUrls: ['./sales-info.component.scss']
})
export class SalesInfoComponent implements OnInit {
salesData;
  constructor() { }

  ngOnInit(): void {
    this.salesData = JSON.parse(localStorage.getItem('exhibitData'));
  }
  closePopup(){
    $('.salesModal').modal('hide');
  }
}
