import { Component, OnInit } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-heightlight',
  templateUrl: './heightlight.component.html',
  styleUrls: ['./heightlight.component.scss']
})
export class HeightlightComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  closePopup(){
    $('.heightlightModal').modal('hide');
  }
}
