import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { AuthService } from '../../app/services/auth.service';
import { FetchDataService } from '../services/fetch-data.service';
@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  constructor(private authServ: FetchDataService,
    private router: Router){}
  canActivate(): boolean {
    if(localStorage.getItem('virtual')){
    return true;
    }else{
      this.router.navigate(['/login'])
      return false
    }
  }
}
