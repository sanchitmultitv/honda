import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FetchDataService } from '../services/fetch-data.service';
import { AuthService } from '../services/auth.service';
function emailDomainValidator(control: FormControl) {
  let email = control.value;
  if (email && email.indexOf("@") != -1) {
    let [_, domain] = email.split("@");
    if (domain !== "gmail.com" && domain !== "multitv.com") {
      return {
        emailDomain: {
          parsedDomain: domain
        }
      }
    }
  }
  return null;
}
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  checked = false;
  checkMessage =false;
  signupForm = new FormGroup({
    first_name: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email,
      Validators.pattern("[^ @]*@[^ @]*"),
      emailDomainValidator]),
    // job_title: new FormControl('', [Validators.required]),
    // accept: new FormControl('', [Validators.required])
  });

  public imagePath;
  imgURL: any;
  public message: string;
  msg;
  colr;
  thanks=false;
  notthanks=true;
  randomPass:any;
  names:any;
  emails:any;
  constructor(private router: Router, private _fd: FetchDataService, private _auth: AuthService) { }

  ngOnInit(): void {

  }

  preview(event) {
    let files = event.target.files;
    if (files.length === 0)
      return;

    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    } else {
      const file = files[0];
      this.signupForm.patchValue({
        image: file
      });
    }

    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }
  }

  isChecked(event){
    this.checked = !this.checked;
  }
  register() {
    if(this.signupForm.get('first_name').value == '' || this.signupForm.get('email').value == ''){
      // this.msg = 'Invalid Input';
    }else{
      if(this.checked){
        var characters = "900000abcdefghiklmnopqrstuvwxyz";  
        var lenString = 8; 
        var randomstring = '';  
        //this.randomPass =  Math.floor(10000000 + Math.random() * 90000000);
        for (var i=0; i<lenString; i++) {  
          var rnum = Math.floor(Math.random() * characters.length);  
          randomstring += characters.substring(rnum, rnum+1);  
      }  
      console.log(randomstring);
      this.randomPass= btoa(randomstring);
        const formData = new FormData();
        formData.append('name', this.signupForm.get('first_name').value);
        formData.append('email', this.signupForm.get('email').value);
        formData.append('category', 'india');
        formData.append('password', this.randomPass);
        
          this._auth.register(formData).subscribe((res:any) => {
            if(res.code ==1){
              this.msg = 'Thank you for registering! Your login password has been sent to your registered email id';
              // setTimeout(() => {
                // this.router.navigate(['/login']);
                // this.thanks = true;
                this.notthanks = false
              // }, 2000);
            }
            else{
              this.msg = res.result;
            }
            this.signupForm.reset();
          });
      }
      else{
        this.checkMessage = true;
      }
     
    }
    
  }
}
